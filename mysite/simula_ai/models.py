from django.db import models
from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # other fields here

    def __str__(self):
        return self.user.username

class Rotulo(models.Model):
    titulo = models.CharField('título', max_length = 30)
    class Meta:
        verbose_name = 'Rótulo'
    def __str__(self):
        return self.titulo

class Pergunta(models.Model):
    texto = models.CharField(max_length = 100)
    rotulos = models.ManyToManyField(Rotulo, blank=True)
    possui_resposta = models.BooleanField(default=False)
    
    #**** retorna o estado da instância na forma de texto
    def __str__(self):
        return ("{0} - {1}").format(self.id, self.texto)


class Alternativa(models.Model):
    texto = models.CharField(max_length = 50)
    pergunta = models.ForeignKey(
        Pergunta, on_delete = models.CASCADE
    )
    correta = models.BooleanField(default=False)
    #**** retorna o estado da instância na forma de texto
    def __str__(self):
        return ("{0} - {1}").format(self.pergunta.texto, self.texto)
    
class Resposta(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    alternativa = models.ForeignKey(Alternativa, on_delete=models.CASCADE)

class Simulado(models.Model):
    titulo = models.CharField(max_length = 30)
    perguntas = models.ManyToManyField(Pergunta)
    alternativas = models.ForeignKey(Alternativa, on_delete=models.CASCADE)
    class Meta:
        verbose_name = 'Simulado'
    def __str__(self):
        return self.titulo

