from django.urls import path
from . import views
from django.views.generic import TemplateView

app_name = 'simula_ai'
urlpatterns = [
    path('', views.index, name='index'),
    path('detalhes/<int:pk>/', views.detalhes.as_view(), name='detalhes'),
    path('resultado', views.resultado, name='resultado'),

]