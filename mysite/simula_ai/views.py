
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
import simula_ai
from .models import Pergunta, Alternativa, Rotulo, Simulado, Resposta
from django.views import View
# Create your views here.


def index(request):
    simulados = Simulado.objects.all()
    template = loader.get_template('simula_ai/index.html')
    context = {'simulados': simulados}
    return HttpResponse(template.render(context, request))

class detalhes(View):
    def get(self, request, pk):
        simulado = get_object_or_404(Simulado, pk=pk)
        perguntas = simulado.perguntas.all()
        alternativas = Alternativa.objects.filter(pergunta__in=perguntas)
        context = {'perguntas': perguntas, 'simulado': simulado, 'alternativas': alternativas}
        return render(request, 'simula_ai/detalhes.html',context )

    def post(self, request, pk):
        simulado = get_object_or_404(Simulado, pk=pk)
        perguntas = simulado.perguntas.all()
        alternativas = Alternativa.objects.filter(pergunta__in=perguntas)
        respostas = Resposta.objects.filter(simulado=simulado)
        context = {'perguntas': perguntas, 'simulado': simulado, 'alternativas': alternativas, 'respostas': respostas}
        return render(request, 'simula_ai/resultado.html', context)
@csrf_exempt
class resultado (View):
    def get(self, request, pk):
        pergunta = get_object_or_404(Pergunta, pk=pk)
        alternativas = pergunta.alternativa_set.all()
        context = {'pergunta': pergunta, 'alternativas': alternativas}
        return render(request, 'simula_ai/resultado.html', context)