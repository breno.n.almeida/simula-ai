from django.contrib import admin
from .models import Pergunta, Alternativa, Rotulo, Simulado, Resposta
# Register your models here.

admin.site.register(Pergunta)
admin.site.register(Alternativa)
admin.site.register(Rotulo)
admin.site.register(Simulado)
admin.site.register(Resposta)

