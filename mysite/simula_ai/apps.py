from django.apps import AppConfig


class SimulaAiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'simula_ai'
